package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/user"
	"strconv"
	"strings"

	"git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcilog.git"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	configPath       = "/etc/cursus/cursus.yaml"
	cursusDataFolder = ""
	timestampFormat  = "2006-01-02 15:04:05"
)

func main() {
	// configure logrus logger
	hcilog.InitializeLog(app.Name, *debugFlag)

	// load config
	if *configFilename != "" {
		configPath = *configFilename
	}
	setUpConfiguration(configPath)

	// overwrite config with env variables
	if os.Getenv("CURSUS_HISTORY") != "" {
		i, err := strconv.Atoi(os.Getenv("CURSUS_HISTORY"))
		if err != nil {
			hcilog.NewErrorWrapper(err).LogError()
		} else {
			serviceConfig.General.SyncFromRemote.History = i
		}
	}

	// set log layout
	if serviceConfig.General.TimestampFormat != "" {
		timestampFormat = serviceConfig.General.TimestampFormat
	}

	// get current user
	usr, err := user.Current()
	if err != nil {
		hcilog.LogError(err)
		os.Exit(1)
	}

	// set paths
	if serviceConfig.Database.Local.DatabasePath == "" {
		serviceConfig.Database.Local.DatabasePath = fmt.Sprintf("%s/.cursus", usr.HomeDir)
		if _, err := os.Stat(serviceConfig.Database.Local.DatabasePath); os.IsNotExist(err) {
			os.Mkdir(serviceConfig.Database.Local.DatabasePath, 0700)
		}
		cursusDataFolder = serviceConfig.Database.Local.DatabasePath
		serviceConfig.Database.Local.DatabasePath += "/cursus.db"
	} else {
		if !strings.Contains(serviceConfig.Database.Local.DatabasePath, ".db") {
			cursusDataFolder = serviceConfig.Database.Local.DatabasePath
			serviceConfig.Database.Local.DatabasePath += "/cursus.db"
		} else {
			split := strings.Split(serviceConfig.Database.Local.DatabasePath, "/")
			cursusDataFolder = strings.Join(split[:len(split)-1], "/")
		}
	}

	// open database
	openDatabases()
	if remdb != nil {
		defer remdb.Close()
	}
	defer db.Close()
	ew := handleDatabases()
	if ew != nil {
		ew.LogError()
		os.Exit(1)
	}

	// handle commands
	kpArgs := kingpin.MustParse(app.Parse(os.Args[1:]))

	switch kpArgs {
	case migrateCommand.FullCommand():
		// migrate old database
		ew = migrateDatabase()
		if ew != nil {
			ew.LogError()
		}
	case saveCommand.FullCommand():
		buf := &bytes.Buffer{}
		n, err := io.Copy(buf, os.Stdin)
		if err != nil {
			hcilog.LogDebug(err.Error())
			os.Exit(0)
		} else if n == 0 || n <= 1 { // command buffer always contains '\n'
			os.Exit(0)
		}
		handleWriteToDatabase(buf)
	case searchCommand.FullCommand():
		if len(*searchCriteriaArg) == 0 {
			getFromDatabase([]string{})
		} else {
			getFromDatabase(*searchCriteriaArg)
		}
	case renameFQDNCommand.FullCommand():
		hcilog.LogInfo(fmt.Sprintf("Renaming %s to %s", *renameSourceFQDNArg, *renameDestinationFQDNArg))
		ew = renameFQDN(*renameSourceFQDNArg, *renameDestinationFQDNArg)
		if ew != nil {
			ew.LogError()
		} else {
			hcilog.LogInfo(fmt.Sprintf("Successfully renamed %s to %s", *renameSourceFQDNArg, *renameDestinationFQDNArg))
		}
	case listFQDNCommand.FullCommand():
		users := []*userStruct{}
		if remdb != nil {
			remtx, err := remdb.Begin()
			if err != nil {
				hcilog.NewErrorWrapper(err).LogError()
			}
			users, ew = getUsers(remtx, nil)
			if ew != nil {
				remtx.Rollback()
				hcilog.NewErrorWrapper(err).LogError()
			}
			err = remtx.Commit()
			if err != nil {
				remtx.Rollback()
				hcilog.NewErrorWrapper(err).LogError()
			}
		}
		if len(users) == 0 {
			tx, err := db.Begin()
			if err != nil {
				hcilog.NewErrorWrapper(err).LogError()
			}
			users, ew = getUsers(tx, nil)
			if ew != nil {
				tx.Rollback()
				ew.LogError()
			}
			err = tx.Commit()
			if err != nil {
				tx.Rollback()
				hcilog.NewErrorWrapper(err).LogError()
			}
		}

		hcilog.LogInfo("Found the following users:")
		for _, u := range users {
			fmt.Printf("- %s %s %s\n", u.UUID, hcichameleon.Red("~").String(), hcichameleon.Lightcyan(u.Name).String())
		}
	case syncCommand.FullCommand():
		if remdb != nil {
			hcilog.LogInfo("Starting syncing from remote")
			ew = sync(true)
			if ew != nil {
				ew.LogError()
			}
			hcilog.LogInfo("Successfully synced from remote")
		} else {
			hcilog.LogWarn("No remote database configured!")
		}
	}
}

func handleWriteToDatabase(buf *bytes.Buffer) {
	oldcur := "Your bashrc is using the old cursus command. Please edit your bashrc and modify it to the new default!"
	incmt := 0
	command := strings.TrimSpace(buf.String())
	spl := strings.Split(command, " ")
	if len(spl) > 0 {
		var err error
		incmt, err = strconv.Atoi(spl[0])
		if err != nil {
			hcilog.LogWarn(oldcur)
			help()
		} else {
			if incmt == 0 {
				hcilog.LogWarn(oldcur)
				help()
			}
		}
	} else {
		hcilog.LogWarn(oldcur)
		help()
	}
	fresh := false
	incmtFile := cursusDataFolder + "/.cursus-incmt"
	dat, err := json.Marshal(&savedIncmtStruct{LastNumber: incmt})
	if err != nil {
		hcilog.NewErrorWrapper(err).LogError()
	}
	if _, err := os.Stat(incmtFile); os.IsNotExist(err) {
		ew := writeToFile(dat, incmtFile)
		if ew != nil {
			return
		}
		fresh = true
	}
	lastIncmtB, ew := readFromFile(incmtFile)
	if ew != nil {
		return
	}
	lastIncmt := &savedIncmtStruct{}
	err = json.Unmarshal(lastIncmtB, &lastIncmt)
	if err != nil {
		hcilog.NewErrorWrapper(err).LogError()
	}
	ew = writeToFile(dat, incmtFile)
	if ew != nil {
		return
	}

	// Remove incremental number from buffer
	command = strings.Replace(command, fmt.Sprintf("%d  ", incmt), "", 1)
	if fresh || incmt > lastIncmt.LastNumber {
		if !strings.Contains(command, "cursus") && command != "his" && command != "cur" && !strings.HasPrefix(command, "his ") && !strings.HasPrefix(command, "cur ") { // Check if command is not cursus or the alias his/cur
			tx, err := db.Begin()
			if err != nil {
				hcilog.NewErrorWrapper(err).LogError()
			}
			if remdb != nil {
				remtx, err := remdb.Begin()
				if err != nil {
					hcilog.NewErrorWrapper(err).LogError()
				}
				ew := writeToDatabase(map[*sql.Tx]bool{tx: false, remtx: true}, []*cursusStruct{{Command: command}}, getHostname())
				if ew != nil {
					tx.Rollback()
					remtx.Rollback()
					ew.LogError()
					os.Exit(1)
				}
				err = remtx.Commit()
				if err != nil {
					hcilog.NewErrorWrapper(err).LogError()
				}
			} else {
				ew := writeToDatabase(map[*sql.Tx]bool{tx: false}, []*cursusStruct{{Command: command}}, getHostname())
				if ew != nil {
					tx.Rollback()
					ew.LogError()
					os.Exit(1)
				}
			}
			err = tx.Commit()
			if err != nil {
				hcilog.NewErrorWrapper(err).LogError()
			}
		}
	}
}
