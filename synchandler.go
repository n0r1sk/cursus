package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"git.app.strabag.com/dev/hostingci/gomodules/hcilog.git"
)

type dbSyncStruct struct {
	User   []*userStruct
	Cursus []*cursusStruct
}

type dbSyncLockStruct struct {
	Timestamp time.Time
}

func sync(userOperation bool) (ew *hcilog.ErrorWrapper) {
	fresh := userOperation
	syncLockFile := cursusDataFolder + "/.cursus-synclock"
	if _, err := os.Stat(syncLockFile); os.IsNotExist(err) {
		dat, err := json.Marshal(&dbSyncLockStruct{Timestamp: time.Now()})
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		ew = writeToFile(dat, syncLockFile)
		if ew != nil {
			return
		}
		fresh = true
	}
	timeb, ew := readFromFile(syncLockFile)
	if ew != nil {
		return
	}
	times := &dbSyncLockStruct{}
	err := json.Unmarshal(timeb, &times)
	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}

	if fresh || times.Timestamp.Add(time.Hour*time.Duration(serviceConfig.General.SyncFromRemote.Timer)).Before(time.Now()) {
		tx, err := db.Begin()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		remtx, err := remdb.Begin()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}

		// update lock file
		err = os.Remove(syncLockFile)
		if err != nil {
			tx.Rollback()
			remtx.Rollback()
			return hcilog.NewErrorWrapper(err)
		}
		dat, err := json.Marshal(&dbSyncLockStruct{Timestamp: time.Now()})
		if err != nil {
			tx.Rollback()
			remtx.Rollback()
			return hcilog.NewErrorWrapper(err)
		}
		ew = writeToFile(dat, syncLockFile)
		if ew != nil {
			tx.Rollback()
			remtx.Rollback()
			return
		}

		var data *dbSyncStruct
		var usersl []*userStruct
		var toDelete []*userStruct
		data, ew = getMySQLSync(map[*sql.Tx]bool{remtx: true})
		if ew != nil {
			tx.Rollback()
			remtx.Rollback()
			return
		}

		if userOperation {
			hcilog.LogInfo("Now syncing users")
		}
		for _, u := range data.User {
			//insert new users to local db
			_, ew = getOrInsertUserDB(map[*sql.Tx]bool{tx: false}, u)
			if ew != nil {
				return
			}
		}

		usersl, ew = getUsers(tx, nil)
		if ew != nil {
			return
		}

		for _, ul := range usersl {
			is := false
			for _, ur := range data.User {
				if ul.Name == ur.Name && ul.UUID == ur.UUID {
					is = true
					break
				}
			}
			if !is {
				toDelete = append(toDelete, ul)
			}
		}

		for _, u := range toDelete {
			// delete users with entries which are renamed on another server
			ew = deleteUser(map[*sql.Tx]bool{tx: false}, u)
			if ew != nil {
				tx.Rollback()
				return
			}
		}

		if userOperation {
			hcilog.LogInfo(fmt.Sprintf("Now inserting cursus data from last %d days. This can take a while. Please stay patient!", serviceConfig.General.SyncFromRemote.History))
		}

		ew = bulkWriteToDatabase(map[*sql.Tx]bool{tx: false}, data.Cursus, getHostname())
		if ew != nil {
			tx.Rollback()
			return
		}

		err = tx.Commit()
		if err != nil {
			tx.Rollback()
			return hcilog.NewErrorWrapper(err)
		}
	}
	return
}

func getMySQLSync(txs map[*sql.Tx]bool) (data *dbSyncStruct, ew *hcilog.ErrorWrapper) {
	remtx := &sql.Tx{}
	for tx, rem := range txs {
		if rem {
			remtx = tx
		}
	}
	users, ew := getUsers(remtx, nil)
	if ew != nil {
		ew.LogError()
	}

	cursus, ew := getCursus(remtx, "WHERE Timestamp > ?", time.Now().AddDate(0, 0, -serviceConfig.General.SyncFromRemote.History))
	if ew != nil {
		remtx.Rollback()
		ew.LogError()
	}

	data = &dbSyncStruct{User: users, Cursus: cursus}
	return
}
