#!/bin/bash
url=$(curl --silent -i -H "Accept: application/json" https://gitlab.com/api/v4/projects/20843292/releases | tr ',' '\n,' | grep "cursus-darwin-amd64.zip" | cut -d ":" -f2- | sed -n 2p | awk -F\" '{print $2}')
config=$(curl --silent https://gitlab.com/n0r1sk/cursus/-/raw/master/src/cursus.yaml)
echo "Downloading binary from $url"
curl -L $url --output /tmp/cursus.zip && \
unzip /tmp/cursus.zip build/cursus-darwin-amd64 -d /tmp/ && \
mv /tmp/build/cursus-darwin-amd64 /usr/local/bin/cursus && \
rm /tmp/cursus.zip && \
chmod 755 /usr/local/bin/cursus && \
if [ ! -f "/etc/cursus/cursus.yaml" ]
then
    sudo mkdir /etc/cursus && \
    echo "$config" | sudo tee -a /etc/cursus/cursus.yaml > /dev/null
fi
check=$(cat ~/.bashrc | grep "cursus save")
if [ $? == 1 ]
then
    echo "export PROMPT_COMMAND=\$PROMPT_COMMAND';(history 1 | cursus save &)'" >> ~/.bashrc
fi
source ~/.bashrc && \
echo "Installation complete!"
