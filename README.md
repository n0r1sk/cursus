![](https://gitlab.com/n0r1sk/cursus/-/raw/master/Cursus-16-9.jpg?inline=false)

# Cursus

Cursus writes your commands into an sqlite db. With cursus you can search for commands and the searched term will be highlighted if it was found.

# How does it look like?


Click on the image to see the [asciinema recording](https://asciinema.org/a/HrimMri1COsDEdMNOGksk2l2G)!

[![asciicast](https://asciinema.org/a/HrimMri1COsDEdMNOGksk2l2G.png)](https://asciinema.org/a/HrimMri1COsDEdMNOGksk2l2G)

# Quickstart

## Linux AMD64 (e.g ubuntu >=20.04)
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-amd64
~~~
## Linux AMD64-Static (e.g ubuntu <=20.04)
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-amd64-static
~~~
## Linux 386
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-386
~~~
## Linux ARM
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-arm
~~~
## Linux ARM64
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-arm64
~~~

## Darwin AMD64
- Currently not supported

# Manual installation and configuration
- Copy `cursus` from the release to /usr/local/bin/

- Edit your .bashrc and add the following to it:
  - `export PROMPT_COMMAND='(history 1 | cursus save &)'`

- Or if you use zshell, edit .zshrc and add this to the bottom:
  - `export PROMPT_COMMAND='(history | tail -n 1 | cursus save &)'`
  - precmd() {eval "$PROMPT_COMMAND"}

- Optional you can set this alias in your .bashrc:
  - `alias cur="cursus search"`

## Overwriting `ctrl+r` Bash shortcut
If you like, you can overwrite the default reverse history search shortcut `ctrl+r` with the following addition to the `.bashrc`:

~~~
bind -x '"\C-r":echo -n "Search for: "; read -e var; cursus search $var'
~~~

# Config
## /etc/cursus/cursus.yaml
~~~yaml
general:
  timestampformat: "2006-01-02 15:04:05" #reference https://pkg.go.dev/time#pkg-constants
  syncfromremote:
    timer: 1 #hour
    history: 30 #days
database:
  hostnamebased: true
  remote:
    useremote: false
    sql:
      user: "user"
      password: 'p@$$w0rd'
      domain: "127.0.0.1"
      port: 3306
      databasename: "XXX"
    googlecloudsql:
      instance: "XXX:XXX:XXX"
      authfilepath: "/path/to/auth/auth.json"
      databasename: "XXX"
  local:
    databasepath: "" #default home folder
~~~
## Explanation
- **TimestampFormat**: You can choose your own layout for how the history timestamp should look like
- **Timer**: This timer runs every x hour to sync data from the remote database
- **History**: Choose how many days back it should sync from remote
- **Hostnamebased**: Save history per fqdn/server or should all fqdn(s)/servers should use the same history (if enabled -f flag for search is available)
- **UseRemote**: Use a remote database for syncing between multiple servers
- **SQL**: Config for MySQL/MariaDB database
  - **User**: Username for DB
  - **Password**: Password for DB
  - **Domain**: IP/Domain of you database
  - **Port**: Port for DB
  - **Databasename**: Databasename for cursus on your DB
- **GoogleCloudSQL**: Use Google CloudSQL as remote database (proxy will be started in cursus)
  - **instance**: Instancename of your google CloudSQL DB
  - **authfilejson**: User authentication file in json format
  - **databasename**: Databasename for cursus on your DB
- **databasename**: Local path with name for your local sqlite database



# Command-Layout
## CURSUS SAVE (used to track your history)

As the example above shows, `cursus save` is used to save your last command into the SQlite database.

## CURSUS SEARCH (search in database)
~~~
usage: cursus search [<flags>] [<criteria>...]

search for a command

Flags:
      --help           Show context-sensitive help (also try --help-long and --help-man).
  -c, --config=CONFIG  Configuration filename. Default: /etc/cursus/cursus.yaml
      --debug          enables debug
  -p, --paste          paste the nth last command from the history to your console
  -e, --execute        execute the nth last command from the history to your console
  -n, --n=1            specify which command should be either pasted or executed. Order is bottom to top (default last=1)
  -f, --fqdn=FQDN      enter a different fqdn to get results from another server

Args:
  [<criteria>]  criteria you want to search for
~~~

## CURSUS Migrate (migrate old v1.X schema to v2.X schema)
~~~
usage: cursus migrate

migrate old database schema to new one

Flags:
      --help           Show context-sensitive help (also try --help-long and --help-man).
  -c, --config=CONFIG  Configuration filename. Default: /etc/cursus/cursus.yaml
      --debug          enables debug
~~~

# Command-Layout - if *useremote* is enabled in config
## CURSUS FQDN List (only available if hostnamebased is enabled | list all different users) 
~~~
usage: cursus fqdn list

list all saved fqdn

Flags:
      --help           Show context-sensitive help (also try --help-long and --help-man).
  -c, --config=CONFIG  Configuration filename. Default: /etc/cursus/cursus.yaml
      --debug          enables debug
~~~

## CURSUS FQDN Rename (rename a fqdn to a existing fqdn)
~~~
usage: cursus fqdn rename <fqdn-source> <fqdn-destination>

Rename a fqdn

Flags:
      --help           Show context-sensitive help (also try --help-long and --help-man).
  -c, --config=CONFIG  Configuration filename. Default: /etc/cursus/cursus.yaml
      --debug          enables debug

Args:
  <fqdn-source>       The source FQDN which should be renamed
  <fqdn-destination>  The destination FQDN defines the new name for the source
~~~

## CURSUS Sync (sync from remote database | will also be scheduled automatically - setting in config)
~~~
usage: cursus sync

Manually sync from remote database

Flags:
      --help           Show context-sensitive help (also try --help-long and --help-man).
  -c, --config=CONFIG  Configuration filename. Default: /etc/cursus/cursus.yaml
      --debug          enables debug
~~~