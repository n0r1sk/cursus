package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"strings"
	"time"

	goauth "golang.org/x/oauth2/google"

	"git.app.strabag.com/dev/hostingci/gomodules/hcilog.git"
	"github.com/GoogleCloudPlatform/cloudsql-proxy/logging"
	"github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/mysql"
	"github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/proxy"
	"github.com/Jeffail/gabs"
	"golang.org/x/oauth2"
)

func cloudSQLInit() *hcilog.ErrorWrapper {
	token := &savedSQLToken{}
	ex, ew := cloudSQLCheckAccessToken(token)
	if ew != nil {
		return ew
	}
	if !ex {
		token, ew = cloudSQLGetAccessToken()
		if ew != nil {
			return ew
		}
		js, err := json.Marshal(token)
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		writeToFile(js, cursusDataFolder+"/.cursus-token")
	}
	logging.DisableLogging()
	proxy.Init(oauth2.NewClient(context.Background(), oauth2.StaticTokenSource(&oauth2.Token{AccessToken: token.AccessToken})), nil, nil)

	sqldb, ew := cloudSQLDial(token.User, token.AccessToken)
	if ew != nil {
		return ew
	}
	remdb = sqldb
	return nil
}

func cloudSQLCheckAccessToken(token *savedSQLToken) (bool, *hcilog.ErrorWrapper) {
	savedtokenb, ew := readFromFile(cursusDataFolder + "/.cursus-token")
	if ew != nil {
		return false, nil
	}
	err := json.Unmarshal(savedtokenb, &token)
	if err != nil {
		return false, hcilog.NewErrorWrapper(err)
	}
	if token.Expiry.Before(time.Now()) {
		return false, nil
	}
	return true, nil
}

func cloudSQLGetAccessToken() (*savedSQLToken, *hcilog.ErrorWrapper) {
	SQLScope := "https://www.googleapis.com/auth/sqlservice.admin"

	creds, err := ioutil.ReadFile(serviceConfig.Database.Remote.GoogleCloudSQL.AuthFilePath)
	if err != nil {
		return nil, hcilog.NewErrorWrapper(err)
	}

	credsParsed, err := gabs.ParseJSON(creds)
	if err != nil {
		return nil, hcilog.NewErrorWrapper(err)
	}
	user := strings.Split(credsParsed.S("client_email").Data().(string), "@")[0]

	cfg, err := goauth.JWTConfigFromJSON(creds, SQLScope)
	if err != nil {
		return nil, hcilog.NewErrorWrapper(err)
	}
	tkn, err := cfg.TokenSource(context.Background()).Token()
	if err != nil {
		return nil, hcilog.NewErrorWrapper(err)
	}
	token := &savedSQLToken{Token: tkn, User: user}
	return token, nil
}

func cloudSQLDial(user, token string) (*sql.DB, *hcilog.ErrorWrapper) {
	cfgM := mysql.Cfg(serviceConfig.Database.Remote.GoogleCloudSQL.Instance, user, token)
	cfgM.AllowCleartextPasswords = true
	cfgM.DBName = serviceConfig.Database.Remote.GoogleCloudSQL.DatabaseName
	cfgM.ParseTime = true

	db, err := mysql.DialCfg(cfgM)
	if err != nil {
		return db, hcilog.NewErrorWrapper(err)
	}
	return db, nil
}
