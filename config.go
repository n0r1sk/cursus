package main

import (
	"io/ioutil"

	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
)

func setUpConfiguration(configFilename string) {
	serviceConfig = &serviceConfigObj{configFilename: configFilename}
	cfgdata, _ := ioutil.ReadFile(configFilename)

	hcicore.UnmarshalYaml2Interface(cfgdata, serviceConfig)
}
