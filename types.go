package main

import (
	"database/sql"
	"time"

	"golang.org/x/oauth2"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app = kingpin.New("cursus", "Stores your history and makes it searchable")

	configFilename = app.Flag("config", "Configuration filename. Default: /etc/cursus/cursus.yaml").Short('c').String()

	saveCommand              = app.Command("save", "Save a command")
	syncCommand              = app.Command("sync", "Manually sync from remote database")
	fqdnCommand              = app.Command("fqdn", "List/Rename fqdn")
	renameFQDNCommand        = fqdnCommand.Command("rename", "Rename a fqdn")
	renameSourceFQDNArg      = renameFQDNCommand.Arg("fqdn-source", "The source FQDN which should be renamed").Required().String()
	renameDestinationFQDNArg = renameFQDNCommand.Arg("fqdn-destination", "The destination FQDN defines the new name for the source").Required().String()
	listFQDNCommand          = fqdnCommand.Command("list", "list all saved fqdn")
	migrateCommand           = app.Command("migrate", "migrate old database schema to new one")
	searchCommand            = app.Command("search", "search for a command")
	searchCriteriaArg        = searchCommand.Arg("criteria", "criteria you want to search for").Strings()
	searchPasteFlag          = searchCommand.Flag("paste", "paste the nth last command from the history to your console").Short('p').Bool()
	searchExecuteFlag        = searchCommand.Flag("execute", "execute the nth last command from the history to your console").Short('e').Bool()
	searchNFlag              = searchCommand.Flag("n", "specify which command should be either pasted or executed. Order is bottom to top (default last=1)").Short('n').Default("1").Int()
	searchFQDNFlag           = searchCommand.Flag("fqdn", "enter a different fqdn to get results from another server").Short('f').String()

	debugFlag     = app.Flag("debug", "enables debug").Bool()
	serviceConfig *serviceConfigObj

	cursusTableName = "Cursus"
	oldTableName    = "history"
	userTableName   = "User"

	db    *sql.DB
	remdb *sql.DB
)

type cursusStruct struct {
	ID        string // historic
	UUID      string
	UUUID     string
	Timestamp time.Time
	Command   string
}

type userStruct struct {
	UUID string
	Name string
}

type savedSQLToken struct {
	*oauth2.Token
	User string
}

type savedIncmtStruct struct {
	LastNumber int
}

type serviceConfigObj struct {
	configFilename string
	General        struct {
		TimestampFormat string
		SyncFromRemote  struct {
			Timer   int
			History int
		}
	}
	Database struct {
		HostnameBased bool
		Remote        struct {
			UseRemote bool
			SQL       struct {
				User         string
				Password     string
				Domain       string
				Port         int
				DatabaseName string
			}
			GoogleCloudSQL struct {
				Instance     string
				AuthFilePath string
				DatabaseName string
			}
		}
		Local struct {
			DatabasePath string
		}
	}
}
