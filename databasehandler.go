package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcilog.git"
)

func openDatabases() {
	var ew *hcilog.ErrorWrapper
	if serviceConfig.Database.Remote.UseRemote {
		if serviceConfig.Database.Remote.GoogleCloudSQL.Instance != "" &&
			serviceConfig.Database.Remote.GoogleCloudSQL.AuthFilePath != "" &&
			serviceConfig.Database.Remote.GoogleCloudSQL.DatabaseName != "" {
			// open cloudsql database
			ew = cloudSQLInit()
			if ew != nil {
				ew.AddAdditionalInfo("Error while connecting to remote database - either fix the problem or remove it from the config to prevent error output")
				ew.LogError()
				os.Exit(1)
			}
		} else if serviceConfig.Database.Remote.SQL.DatabaseName != "" &&
			serviceConfig.Database.Remote.SQL.Domain != "" &&
			serviceConfig.Database.Remote.SQL.User != "" {
			// open sql database
			dsn := ""
			if serviceConfig.Database.Remote.SQL.Password != "" {
				dsn = fmt.Sprintf("%s:%s@tcp(%s)/%s", serviceConfig.Database.Remote.SQL.User,
					serviceConfig.Database.Remote.SQL.Password,
					serviceConfig.Database.Remote.SQL.Domain+hcicore.IfThenElse(serviceConfig.Database.Remote.SQL.Port != 0, fmt.Sprintf(":%d", serviceConfig.Database.Remote.SQL.Port), "").(string),
					serviceConfig.Database.Remote.SQL.DatabaseName)
			} else {
				dsn = fmt.Sprintf("%s@tcp(%s)/%s", serviceConfig.Database.Remote.SQL.User,
					serviceConfig.Database.Remote.SQL.Domain+hcicore.IfThenElse(serviceConfig.Database.Remote.SQL.Port != 0, fmt.Sprintf(":%d", serviceConfig.Database.Remote.SQL.Port), "").(string),
					serviceConfig.Database.Remote.SQL.DatabaseName)
			}
			ew = openDatabase("mysql", dsn, true)
			if ew != nil {
				ew.AddAdditionalInfo("Error while connecting to remote database - either fix the problem or remove it from the config to prevent error output")
				ew.LogError()
			}
		}
	}

	ew = openDatabase("sqlite3", serviceConfig.Database.Local.DatabasePath+"?_busy_timeout=100000&_journal=WAL&_sync=NORMAL&cache=shared", false)
	if ew != nil {
		ew.LogError()
		os.Exit(1)
	}
}

func openDatabase(driverName string, dataSourceName string, remote bool) *hcilog.ErrorWrapper {
	var err error

	sqldb, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}

	sqldb.SetMaxOpenConns(20)
	sqldb.SetMaxIdleConns(20)

	if remote {
		sqldb.SetConnMaxLifetime(time.Minute * 5)
		remdb = sqldb
	} else {
		db = sqldb
	}

	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	err = sqldb.PingContext(ctx)
	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}
	if !remote {
		_, err = sqldb.Exec("PRAGMA journal_mode=WAL")
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
	}

	return nil
}

func migrateDatabase() *hcilog.ErrorWrapper {
	var timestamp string
	var command string
	var id string
	var remtx *sql.Tx
	count := 200
	txs := map[*sql.Tx]bool{}

	tx, err := db.Begin()
	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}
	txs[tx] = false
	if remdb != nil {
		remtx, err = remdb.Begin()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		txs[remtx] = true
	}

	stmt, err := tx.Prepare("SELECT id, timestamp, command FROM " + oldTableName)
	if err != nil {
		if remtx != nil {
			remtx.Rollback()
		}
		tx.Rollback()
		ew := hcilog.NewErrorWrapper(err)
		if strings.Contains(ew.GetError().Error(), "doesn't exist") || strings.Contains(ew.GetError().Error(), "no such table") {
			hcilog.LogInfo("Finished migration!")
			return nil
		}
		return ew
	}
	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		if remtx != nil {
			remtx.Rollback()
		}
		tx.Rollback()
		ew := hcilog.NewErrorWrapper(err)
		if strings.Contains(ew.GetError().Error(), "doesn't exist") || strings.Contains(ew.GetError().Error(), "no such table") {
			hcilog.LogInfo("Finished migration!")
			return nil
		}
		return ew
	}

	commandsToMigrate := []*cursusStruct{}

	for rows.Next() {
		if len(commandsToMigrate) == count {
			break
		}
		err := rows.Scan(&id, &timestamp, &command)
		if err != nil {
			if remtx != nil {
				remtx.Rollback()
			}
			tx.Rollback()
			return hcilog.NewErrorWrapper(err)
		}
		i, err := strconv.ParseInt(timestamp, 10, 64)
		if err != nil {
			if remtx != nil {
				remtx.Rollback()
			}
			tx.Rollback()
			return hcilog.NewErrorWrapper(err)
		}
		t := time.Unix(i, 0)
		commandsToMigrate = append(commandsToMigrate, &cursusStruct{Command: command, ID: id, Timestamp: t})
	}
	rows.Close()

	if len(commandsToMigrate) == 0 {
		hcilog.LogInfo("Finished migration!")
		ew := executeStatement(txs, "DROP TABLE "+oldTableName)
		if ew != nil {
			if remtx != nil {
				remtx.Rollback()
			}
			tx.Rollback()
			ew.LogError()
			return ew
		}
		return nil
	}

	hcilog.LogInfo("Please stay patient - migrating database")

	ew := writeToDatabase(txs, commandsToMigrate, getHostname())
	if ew != nil {
		if remtx != nil {
			remtx.Rollback()
		}
		tx.Rollback()
		ew.LogError()
		return ew
	}

	ids := []string{}

	for _, c := range commandsToMigrate {
		ids = append(ids, c.ID)
	}

	ew = executeStatement(txs, fmt.Sprintf("DELETE FROM "+oldTableName+" WHERE id IN (%s)", strings.Join(ids, ", ")))
	if ew != nil {
		if remtx != nil {
			remtx.Rollback()
		}
		tx.Rollback()
		ew.LogError()
		return ew
	}
	err = tx.Commit()
	if err != nil {
		if remtx != nil {
			remtx.Rollback()
		}
		tx.Rollback()
		return hcilog.NewErrorWrapper(err)
	}
	if remtx != nil {
		err = remtx.Commit()
		if err != nil {
			remtx.Rollback()
			return hcilog.NewErrorWrapper(err)
		}
	}
	hcilog.LogInfo(fmt.Sprintf("Please stay patient - migrated %d entries", count))

	return migrateDatabase()
}

func renameFQDN(source, destination string) (ew *hcilog.ErrorWrapper) {
	var remtx *sql.Tx
	txs := map[*sql.Tx]bool{}
	if source == destination {
		return hcilog.NewErrorWrapper(errors.New("Cannot rename to the same destination!"))
	}
	tx, err := db.Begin()
	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}
	txs[tx] = false
	if remdb != nil {
		remtx, err = remdb.Begin()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		txs[remtx] = true
	}

	suser, ew := getUsers(tx, &userStruct{Name: source})
	if ew != nil {
		tx.Rollback()
		return
	}
	duser, ew := getUsers(tx, &userStruct{Name: destination})
	if ew != nil {
		tx.Rollback()
		return
	}
	if len(suser) == 0 {
		tx.Rollback()
		return hcilog.NewErrorWrapper(errors.New("Source user not found"))
	}
	if len(duser) == 0 {
		tx.Rollback()
		return hcilog.NewErrorWrapper(errors.New("Destination user not found"))
	}
	cursus, ew := getCursus(tx, "WHERE UUUID = ?", suser[0].UUID)
	if ew != nil {
		tx.Rollback()
		return
	}

	cursusToWrite := []*cursusStruct{}
	for _, c := range cursus {
		c.UUUID = duser[0].UUID
		cursusToWrite = append(cursusToWrite, c)
	}

	ew = deleteUser(txs, &userStruct{UUID: suser[0].UUID})
	if ew != nil {
		tx.Rollback()
		if remtx != nil {
			remtx.Rollback()
		}
		return
	}
	ew = bulkWriteToDatabase(txs, cursusToWrite, destination)
	if ew != nil {
		tx.Rollback()
		if remtx != nil {
			remtx.Rollback()
		}
		return
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return hcilog.NewErrorWrapper(err)
	}
	if remtx != nil {
		err = remtx.Commit()
		if err != nil {
			remtx.Rollback()
			return hcilog.NewErrorWrapper(err)
		}
	}

	return
}

func deleteUser(txs map[*sql.Tx]bool, user *userStruct) (ew *hcilog.ErrorWrapper) {
	query := "DELETE FROM " + userTableName + " WHERE "
	if user.UUID != "" {
		query += "UUID = ?"
		ew = executeStatement(txs, query, user.UUID)
	} else {
		query += "Name = ?"
		ew = executeStatement(txs, query, user.Name)
	}
	return
}

func getFromDatabase(query []string) {
	searchQuery := "SELECT c.Timestamp, c.Command FROM " + cursusTableName + " c"
	if serviceConfig.Database.HostnameBased {
		searchQuery = "SELECT c.Timestamp, c.Command, u.Name FROM " + cursusTableName + " c"
	}
	searchQuery += " INNER JOIN " + userTableName + " u ON c.UUUID = u.UUID "
	likeQuery := ""
	if len(query) != 0 {
		searchQuery += " WHERE "

		for i, v := range query {
			likeQuery += fmt.Sprintf("c.Command LIKE '%%%s%%'", v)
			if i < len(query)-1 {
				likeQuery += " AND "
			}
		}

		likeQuery += fmt.Sprintf(" AND u.Name LIKE '%%%s%%'", hcicore.IfThenElse(*searchFQDNFlag != "", *searchFQDNFlag, getHostname()).(string))

		searchQuery += likeQuery
	} else {
		likeQuery += fmt.Sprintf(" WHERE u.Name LIKE '%%%s%%'", hcicore.IfThenElse(*searchFQDNFlag != "", *searchFQDNFlag, getHostname()).(string))
		searchQuery += likeQuery
	}

	tx, err := db.Begin()
	if err != nil {
		tx.Rollback()
		hcilog.NewErrorWrapper(err).LogError()
		help()
	}
	stmt, err := tx.Prepare(searchQuery)
	if err != nil {
		tx.Rollback()
		hcilog.NewErrorWrapper(err).LogError()
		help()
	}
	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		tx.Rollback()
		hcilog.NewErrorWrapper(err).LogError()
		help()
	}

	var timestamp time.Time
	var name string
	var command string
	var unformatted string
	var commands []string

	for rows.Next() {
		if serviceConfig.Database.HostnameBased {
			err = rows.Scan(&timestamp, &command, &name)
			if err != nil {
				tx.Rollback()
				hcilog.NewErrorWrapper(err).LogError()
				help()
			}
		} else {
			err = rows.Scan(&timestamp, &command)
			if err != nil {
				tx.Rollback()
				hcilog.NewErrorWrapper(err).LogError()
				help()
			}
		}
		unformatted = command
		if !*searchPasteFlag && !*searchExecuteFlag {
			if len(query) != 0 {
				for _, x := range query {
					r := regexp.MustCompile("(?i)" + x)
					matches := r.FindAllString(command, -1)
					umatches := []string{}
					for _, y := range matches {
						if !hcicore.StringSliceContains(umatches, y, true) {
							umatches = append(umatches, y)
							command = strings.Replace(command, y, hcichameleon.Lightgreen(y).String(), -1)
						}
					}
				}
			}
			if serviceConfig.Database.HostnameBased && getHostname() != name {
				fmt.Printf("[%s] %s %s %s", hcichameleon.Lightblue(timestamp.Format(timestampFormat)).String(), hcichameleon.Lightcyan(name).String(), hcichameleon.Yellow("~").String(), hcicore.IfThenElse(strings.HasSuffix(command, "\n"), command, command+"\n").(string))
			} else {
				fmt.Printf("[%s] %s", hcichameleon.Lightblue(timestamp.Format(timestampFormat)).String(), hcicore.IfThenElse(strings.HasSuffix(command, "\n"), command, command+"\n").(string))
			}
		}
		commands = append(commands, unformatted)
	}
	rows.Close()
	tx.Commit()

	if *searchPasteFlag {
		commands = hcicore.StringSliceFlip(commands)
		pasteToConsole(commands[*searchNFlag-1])
	}

	if *searchExecuteFlag {
		commands = hcicore.StringSliceFlip(commands)
		executeCommand(commands[*searchNFlag-1])
	}
}

func writeToDatabase(txs map[*sql.Tx]bool, cursus []*cursusStruct, hostname string) *hcilog.ErrorWrapper {
	uuuid, ew := getOrInsertUserDB(txs, &userStruct{Name: hostname})
	if ew != nil {
		return ew
	}
	withTimestamps := hcicore.IfThenElse(cursus[0].Timestamp.IsZero(), false, true).(bool)
	valueStrings := make([]string, 0, len(cursus))
	valueArgs := make([]interface{}, 0, len(cursus)*hcicore.IfThenElse(withTimestamps, 4, 3).(int))
	for _, c := range cursus {
		if c.UUID == "" {
			uuid, err := hcicore.GetUUIDString()
			if err != nil {
				return hcilog.NewErrorWrapper(err)
			}
			c.UUID = uuid
		}
		if c.UUUID == "" {
			c.UUUID = uuuid
		}
		if withTimestamps {
			valueStrings = append(valueStrings, "(?, ?, ?, ?)")
			valueArgs = append(valueArgs, c.UUID)
			valueArgs = append(valueArgs, c.UUUID)
			valueArgs = append(valueArgs, c.Timestamp)
			valueArgs = append(valueArgs, c.Command)
		} else {
			valueStrings = append(valueStrings, "(?, ?, ?)")
			valueArgs = append(valueArgs, c.UUID)
			valueArgs = append(valueArgs, c.UUUID)
			valueArgs = append(valueArgs, c.Command)
		}
	}
	for tx, rem := range txs {
		sqlites := hcicore.IfThenElse(!rem, "OR IGNORE", "").(string)
		if !withTimestamps {
			ew = executeStatement(map[*sql.Tx]bool{tx: rem}, fmt.Sprintf(`
			INSERT `+sqlites+` INTO `+cursusTableName+`(UUID, UUUID, Command) VALUES %s
		`, strings.Join(valueStrings, ",")), valueArgs...)
		} else {
			ew = executeStatement(map[*sql.Tx]bool{tx: rem}, fmt.Sprintf(`
			INSERT `+sqlites+` INTO `+cursusTableName+`(UUID, UUUID, Timestamp, Command) VALUES %s
		`, strings.Join(valueStrings, ",")), valueArgs...)
		}
	}
	if ew != nil {
		return ew
	}

	return nil
}

func getCursus(tx *sql.Tx, userquery string, args ...interface{}) (cursus []*cursusStruct, ew *hcilog.ErrorWrapper) {
	searchQuery := "SELECT UUID, UUUID, Timestamp, Command FROM " + cursusTableName + " " + userquery
	stmt, err := tx.Prepare(searchQuery)
	if err != nil {
		tx.Rollback()
		ew = hcilog.NewErrorWrapper(err)
		return
	}
	rows, err := stmt.Query(args...)
	if err != nil {
		tx.Rollback()
		ew = hcilog.NewErrorWrapper(err)
		return
	}

	var uuid string
	var uuuid string
	var timestamp time.Time
	var command string

	for rows.Next() {
		err = rows.Scan(&uuid, &uuuid, &timestamp, &command)
		if err != nil {
			tx.Rollback()
			ew = hcilog.NewErrorWrapper(err)
			return
		}
		cus := &cursusStruct{UUID: uuid, UUUID: uuuid, Timestamp: timestamp, Command: command}
		cursus = append(cursus, cus)
	}
	stmt.Close()
	rows.Close()
	return
}

func getUsers(tx *sql.Tx, userquery *userStruct) (users []*userStruct, ew *hcilog.ErrorWrapper) {
	queryextend := ""
	if userquery != nil && (userquery.Name != "" || userquery.UUID != "") {
		queryextend = " WHERE "
		if userquery.Name != "" {
			if userquery.UUID != "" {
				queryextend += "Name = ? AND UUID = ?"
			} else {
				queryextend += "Name = ?"
			}
		} else {
			queryextend += "UUID = ?"
		}
	}

	stmt, err := tx.Prepare("SELECT UUID, Name FROM " + userTableName + queryextend)
	if err != nil {
		tx.Rollback()
		ew = hcilog.NewErrorWrapper(err)
		return
	}
	defer stmt.Close()
	var rows *sql.Rows

	if queryextend == "" {
		rows, err = stmt.Query()
	} else {
		if userquery.Name != "" && userquery.UUID != "" {
			rows, err = stmt.Query(userquery.Name, userquery.UUID)
		} else {
			rows, err = stmt.Query(hcicore.IfThenElse(userquery.Name != "", userquery.Name, userquery.UUID))
		}
	}
	if err != nil {
		tx.Rollback()
		ew = hcilog.NewErrorWrapper(err)
		return
	}

	users = []*userStruct{}

	for rows.Next() {
		user := &userStruct{}
		err := rows.Scan(&user.UUID, &user.Name)
		if err != nil {
			tx.Rollback()
			ew = hcilog.NewErrorWrapper(err)
			return
		}
		users = append(users, user)
	}
	rows.Close()
	return
}

func getOrInsertUserDB(txs map[*sql.Tx]bool, user *userStruct) (string, *hcilog.ErrorWrapper) {
	var uuuid string
	for tx := range txs {
		userS, ew := getUsers(tx, user)
		if ew != nil {
			tx.Rollback()
			return "", ew
		}
		if len(userS) > 0 {
			uuuid = userS[0].UUID
		}
		if uuuid != "" {
			break
		}
	}

	if uuuid == "" {
		if user.UUID == "" {
			var err error
			uuuid, err = hcicore.GetUUIDString()
			if err != nil {
				return "", hcilog.NewErrorWrapper(err)
			}
			user.UUID = uuuid
		}
	} else {
		user.UUID = uuuid
	}

	var remtx *sql.Tx
	var ltx *sql.Tx
	for tx, rem := range txs {
		if rem {
			remtx = tx
		} else {
			ltx = tx
		}
	}

	if ltx != nil {
		ew := executeStatement(map[*sql.Tx]bool{ltx: false}, `
		INSERT OR IGNORE INTO `+userTableName+`(UUID, Name) VALUES (?, ?)
		`, user.UUID, user.Name)
		if ew != nil {
			ltx.Rollback()
			ew.LogError()
			return "", ew
		}
	}

	if remtx != nil {
		ew := executeStatement(map[*sql.Tx]bool{remtx: true}, `
		INSERT INTO `+userTableName+`(UUID, Name) VALUES (?, ?) ON DUPLICATE KEY UPDATE Name=Name
		`, user.UUID, user.Name)
		if ew != nil {
			remtx.Rollback()
			ew.LogError()
			return "", ew
		}
	}

	return uuuid, nil
}

func executeStatement(txs map[*sql.Tx]bool, statement string, args ...interface{}) (ew *hcilog.ErrorWrapper) {
	for tx, rem := range txs {
		stmt, err := tx.Prepare(statement)
		if err != nil {
			ew = handleErrorExecuteStatement(tx, rem, hcilog.NewErrorWrapper(err))
			if ew != nil {
				stmt.Close()
				tx.Rollback()
				return
			}
		}

		_, err = stmt.Exec(args...)
		if err != nil {
			ew = handleErrorExecuteStatement(tx, rem, hcilog.NewErrorWrapper(err))
			if ew != nil {
				stmt.Close()
				tx.Rollback()
				return
			}
		}
		stmt.Close()
	}
	return
}

func handleErrorExecuteStatement(tx *sql.Tx, rem bool, ew *hcilog.ErrorWrapper) *hcilog.ErrorWrapper {
	if rem {
		ew.AddAdditionalInfo("Remote Database not reachable!")
		ew.LogError()
		return nil
	} else {
		return ew
	}
}

func createSQLTables(txs map[*sql.Tx]bool) *hcilog.ErrorWrapper {
	ew := executeStatement(txs, `
		CREATE TABLE IF NOT EXISTS `+userTableName+` (
			UUID VARCHAR(36) NOT NULL PRIMARY KEY,
			Name VARCHAR(128) NOT NULL
		)
	`)
	if ew != nil {
		return ew
	}
	ew = executeStatement(txs, `
		CREATE TABLE IF NOT EXISTS `+cursusTableName+` (
			UUID VARCHAR(36) NOT NULL PRIMARY KEY, 
			UUUID VARCHAR(36) NOT NULL,
			Timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			Command BLOB NOT NULL,
			FOREIGN KEY (UUUID) REFERENCES `+userTableName+` (UUID) ON DELETE CASCADE
		)`)
	if ew != nil {
		return ew
	}
	return nil
}

func handleDatabases() (ew *hcilog.ErrorWrapper) {
	tx, err := db.Begin()
	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}
	if remdb != nil {
		remtx, err := remdb.Begin()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		ew = createSQLTables(map[*sql.Tx]bool{tx: false, remtx: true})
		if ew != nil {
			tx.Rollback()
			remtx.Rollback()
			return
		}
		err = remtx.Commit()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		err = tx.Commit()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		//sync
		ew = sync(false)
		if ew != nil {
			return
		}
	} else {
		ew = createSQLTables(map[*sql.Tx]bool{tx: false})
		if ew != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
	}
	return
}
