package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	chameleon "git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcilog.git"
	"github.com/atotto/clipboard"
)

func help() {
	hcilog.LogWarn("Did you extend your shell?")
	hcilog.LogWarn("For " + chameleon.Lightblue("zsh").String() + ": ")
	hcilog.LogWarn("\texport PROMPT_COMMAND='(history 1 | tail -n 1 | cursus save &)'" +
		"\n\tprecmd() {eval \"$PROMPT_COMMAND\"}")
	hcilog.LogWarn("For " + chameleon.Lightblue("bash").String() + ": ")
	hcilog.LogWarn("\texport PROMPT_COMMAND='(history 1 | cursus save &)'")
	hcilog.LogWarn("Please restart all your sessions after editing your bashrc")
	os.Exit(1)
}

func pasteToConsole(command string) {
	command = strings.TrimSuffix(command, "\n")
	clipboard.WriteAll(command)
	hcilog.LogInfo(fmt.Sprintf("The command has been pasted to your clipboard! (STRG+SHIFT+V)\n- %s", command))
}

func executeCommand(command string) {
	command = strings.TrimSuffix(command, "\n")
	hcilog.LogInfo("Executing the command - " + chameleon.Green(command).String())
	//command = "shopt -s expand_aliases;cat ~/.bashrc | grep '^alias' > /tmp/cur_exec_aliases;source /tmp/cur_exec_aliases;\n" + command
	_, code, err := hcicore.ExecuteSingleCommand("bash", "-c", command+" > /proc/"+fmt.Sprintf("%d", os.Getppid())+"/fd/0")
	if code != 0 && code != -1 {
		if err != nil {
			hcilog.LogError(fmt.Errorf("%s\n\n%s %s", chameleon.Red("Command Execution Error").String(), chameleon.Lightred(fmt.Sprintf("ERROR (%d):", code)).String(), err.Error()))
		}
	} else {
		if err != nil {
			hcilog.LogInfo(fmt.Sprintf("\n%s", err.Error()))
		}
	}

	hcilog.LogInfo("Command executed!")
}

func writeToFile(data []byte, path string) *hcilog.ErrorWrapper {
	file, err := os.Create(path)

	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}
	defer file.Close()

	_, err = file.Write(data)

	if err != nil {
		return hcilog.NewErrorWrapper(err)
	}
	return nil
}

func readFromFile(path string) ([]byte, *hcilog.ErrorWrapper) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, hcilog.NewErrorWrapper(err)
	}
	return data, nil
}

func getHostname() string {
	if !serviceConfig.Database.HostnameBased {
		return "default"
	}
	cmd := exec.Command("/bin/hostname", "-f")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		hcilog.NewErrorWrapper(err).LogError()
	}
	fqdn := out.String()
	fqdn = strings.TrimSuffix(fqdn, "\n") // removing EOL
	if fqdn == "" {
		return "default"
	}
	return fqdn
}

func bulkWriteToDatabase(txs map[*sql.Tx]bool, cursus []*cursusStruct, hostname string) (ew *hcilog.ErrorWrapper) {
	i1 := 100
	for i := 0; i <= len(cursus); i += 100 {
		i1 += 100
		if i1 > len(cursus) {
			i1 = len(cursus)
			if i1 == i {
				break
			}
		}
		ew = writeToDatabase(txs, cursus[i:i1], hostname)
		if ew != nil {
			return
		}
	}
	return
}
