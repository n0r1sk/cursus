// Package hcichameleon gives a golang module colors
package hcichameleon

import (
	"fmt"
	"runtime"
)

var (
	cfcolor = make(map[string]string)
)

// Chameleon struct
type Chameleon struct {
	value interface{}
	color string
	end   string
}

// String - Chameleon's to String Method
func (c Chameleon) String() string {
	if c.color != "" {
		return fmt.Sprintf("%s%v%s", c.color, c.value, c.end)
	}
	return fmt.Sprintf("%v", c.value)
}

// Value will get Chameleon object's value
func (c Chameleon) Value() interface{} {
	return c.value
}

// The retun function
func ret(color string, s interface{}) Chameleon {
	if runtime.GOOS == "windows" {
		return Chameleon{value: s}
	}
	switch s := s.(type) {
	case Chameleon:
		return s.addColor(color)
	default:
		return Chameleon{value: s, color: color, end: "\x1b[0m"}
	}
}

// Bold returns the string in bold
func Bold(s interface{}) Chameleon {
	return ret("\x1b[1m", s)
}

// Underlined returns the string underlined
func Underlined(s interface{}) Chameleon {
	return ret("\x1b[4m", s)
}

// Dim returns the string dimmed
func Dim(s interface{}) Chameleon {
	return ret("\x1b[2m", s)
}

// Inverted returns the string color inverted
func Inverted(s interface{}) Chameleon {
	return ret("\x1b[7m", s)
}

// Hidden returns the string in hidden format
func Hidden(s interface{}) Chameleon {
	return ret("\x1b[8m", s)
}

// Foregroundcolors

// Black returns the string in black
func Black(s interface{}) Chameleon {
	return ret("\x1b[30m", s)
}

// Red returns the string in red
func Red(s interface{}) Chameleon {
	return ret("\x1b[31m", s)
}

// Green returns the string in green
func Green(s interface{}) Chameleon {
	return ret("\x1b[32m", s)
}

// Yellow returns the string in yellow
func Yellow(s interface{}) Chameleon {
	return ret("\x1b[33m", s)
}

// Blue returns the string in blue
func Blue(s interface{}) Chameleon {
	return ret("\x1b[34m", s)
}

// Magenta returns the string in magenta
func Magenta(s interface{}) Chameleon {
	return ret("\x1b[35m", s)
}

// Cyan returns the string in cyan
func Cyan(s interface{}) Chameleon {
	return ret("\x1b[36m", s)
}

// Lightgray returns the string in light gray
func Lightgray(s interface{}) Chameleon {
	return ret("\x1b[37m", s)
}

// Darkgray returns the string in dark gray
func Darkgray(s interface{}) Chameleon {
	return ret("\x1b[90m", s)
}

// Lightred returns the string in light red
func Lightred(s interface{}) Chameleon {
	return ret("\x1b[91m", s)
}

// Lightgreen returns the string in light green
func Lightgreen(s interface{}) Chameleon {
	return ret("\x1b[92m", s)
}

// Lightyellow returns the string in light yellow
func Lightyellow(s interface{}) Chameleon {
	return ret("\x1b[93m", s)
}

// Lightblue returns the string in light blue
func Lightblue(s interface{}) Chameleon {
	return ret("\x1b[94m", s)
}

// Lightmagenta returns the string in light magenta
func Lightmagenta(s interface{}) Chameleon {
	return ret("\x1b[95m", s)
}

// Lightcyan returns the string in light cyan
func Lightcyan(s interface{}) Chameleon {
	return ret("\x1b[96m", s)
}

// White returns the string in white
func White(s interface{}) Chameleon {
	return ret("\x1b[97m", s)
}

// Backgroundcolors

// BBlack returns the string in background black
func BBlack(s interface{}) Chameleon {
	return ret("\x1b[40m", s)
}

// BRed returns the string in background red
func BRed(s interface{}) Chameleon {
	return ret("\x1b[41m", s)
}

// BGreen returns the string in background green
func BGreen(s interface{}) Chameleon {
	return ret("\x1b[42m", s)
}

// BYellow returns the string in background yellow
func BYellow(s interface{}) Chameleon {
	return ret("\x1b[43m", s)
}

// BBlue returns the string in background blue
func BBlue(s interface{}) Chameleon {
	return ret("\x1b[44m", s)
}

// BMagenta returns the string in background magenta
func BMagenta(s interface{}) Chameleon {
	return ret("\x1b[45m", s)
}

// BCyan returns the string in background cyan
func BCyan(s interface{}) Chameleon {
	return ret("\x1b[46m", s)
}

// BLightgray returns the string in background light gray
func BLightgray(s interface{}) Chameleon {
	return ret("\x1b[47m", s)
}

// BDarkgray returns the string in background dark gray
func BDarkgray(s interface{}) Chameleon {
	return ret("\x1b[100m", s)
}

// BLightred returns the string in background light red
func BLightred(s interface{}) Chameleon {
	return ret("\x1b[101m", s)
}

// BLightgreen returns the string in background light green
func BLightgreen(s interface{}) Chameleon {
	return ret("\x1b[102m", s)
}

// BLightyellow returns the string in background light yellow
func BLightyellow(s interface{}) Chameleon {
	return ret("\x1b[103m", s)
}

// BLightblue returns the string in background light blue
func BLightblue(s interface{}) Chameleon {
	return ret("\x1b[104m", s)
}

// BLightmagenta returns the string in background light magenta
func BLightmagenta(s interface{}) Chameleon {
	return ret("\x1b[105m", s)
}

// BLightcyan returns the string in background light cyan
func BLightcyan(s interface{}) Chameleon {
	return ret("\x1b[106m", s)
}

// BWhite returns the string in background white
func BWhite(s interface{}) Chameleon {
	return ret("\x1b[107m", s)
}

// Customcolor

// AddCustomColor add a custom color
func AddCustomColor(colorname, color string) {
	cfcolor[colorname] = fmt.Sprintf("\033[" + color)
}

// CustomColor returns a custom color
func CustomColor(colorname string, s interface{}) Chameleon {
	return ret(cfcolor[colorname], s)
}

// addcolor
func (c Chameleon) addColor(color string) Chameleon {
	c.color += color
	return c
}

// Bold returns the string in bold
func (c Chameleon) Bold() Chameleon {
	return c.addColor("\x1b[1m")
}

// Underlined returns the string underlined
func (c Chameleon) Underlined() Chameleon {
	return c.addColor("\x1b[4m")
}

// Dim returns the string dimmed
func (c Chameleon) Dim() Chameleon {
	return c.addColor("\x1b[2m")
}

// Inverted returns the string inverted
func (c Chameleon) Inverted() Chameleon {
	return c.addColor("\x1b[7m")
}

// Hidden returns the string hidden
func (c Chameleon) Hidden() Chameleon {
	return c.addColor("\x1b[8m")
}

// Black returns the string in black
func (c Chameleon) Black() Chameleon {
	return c.addColor("\x1b[30m")
}

// Red returns the string in red
func (c Chameleon) Red() Chameleon {
	return c.addColor("\x1b[31m")
}

// Green returns the string in green
func (c Chameleon) Green() Chameleon {
	return c.addColor("\x1b[32m")
}

// Yellow returns the string in yellow
func (c Chameleon) Yellow() Chameleon {
	return c.addColor("\x1b[33m")
}

// Blue returns the string in blue
func (c Chameleon) Blue() Chameleon {
	return c.addColor("\x1b[34m")
}

// Magenta returns the string in magenta
func (c Chameleon) Magenta() Chameleon {
	return c.addColor("\x1b[35m")
}

// Cyan returns the string in cyan
func (c Chameleon) Cyan() Chameleon {
	return c.addColor("\x1b[36m")
}

// Lightgray returns the string in light gray
func (c Chameleon) Lightgray() Chameleon {
	return c.addColor("\x1b[37m")
}

// Darkgray returns the string in dark gray
func (c Chameleon) Darkgray() Chameleon {
	return c.addColor("\x1b[90m")
}

// Lightred returns the string in light red
func (c Chameleon) Lightred() Chameleon {
	return c.addColor("\x1b[91m")
}

// Lightgreen returns the string in light green
func (c Chameleon) Lightgreen() Chameleon {
	return c.addColor("\x1b[92m")
}

// Lightyellow returns the string in light yellow
func (c Chameleon) Lightyellow() Chameleon {
	return c.addColor("\x1b[93m")
}

// Lightblue returns the string in light blue
func (c Chameleon) Lightblue() Chameleon {
	return c.addColor("\x1b[94m")
}

// Lightmagenta returns the string in light magenta
func (c Chameleon) Lightmagenta() Chameleon {
	return c.addColor("\x1b[95m")
}

// Lightcyan returns the string in light cyan
func (c Chameleon) Lightcyan() Chameleon {
	return c.addColor("\x1b[96m")
}

// White returns the string in white
func (c Chameleon) White() Chameleon {
	return c.addColor("\x1b[97m")
}

// BBlack returns the string in background black
func (c Chameleon) BBlack() Chameleon {
	return c.addColor("\x1b[40m")
}

// BRed returns the string in background red
func (c Chameleon) BRed() Chameleon {
	return c.addColor("\x1b[41m")
}

// BGreen returns the string in background green
func (c Chameleon) BGreen() Chameleon {
	return c.addColor("\x1b[42m")
}

// BYellow returns the string in background yellow
func (c Chameleon) BYellow() Chameleon {
	return c.addColor("\x1b[43m")
}

// BBlue returns the string in background blue
func (c Chameleon) BBlue() Chameleon {
	return c.addColor("\x1b[44m")
}

// BMagenta returns the string in background magenta
func (c Chameleon) BMagenta() Chameleon {
	return c.addColor("\x1b[45m")
}

// BCyan returns the string in background cyan
func (c Chameleon) BCyan() Chameleon {
	return c.addColor("\x1b[46m")
}

// BLightgray returns the string in background light gray
func (c Chameleon) BLightgray() Chameleon {
	return c.addColor("\x1b[47m")
}

// BDarkgray returns the string in background dark gray
func (c Chameleon) BDarkgray() Chameleon {
	return c.addColor("\x1b[100m")
}

// BLightred returns the string in background light red
func (c Chameleon) BLightred() Chameleon {
	return c.addColor("\x1b[101m")
}

// BLightgreen returns the string in background light green
func (c Chameleon) BLightgreen() Chameleon {
	return c.addColor("\x1b[102m")
}

// BLightyellow returns the string in background light yellow
func (c Chameleon) BLightyellow() Chameleon {
	return c.addColor("\x1b[103m")
}

// BLightblue returns the string in background light blue
func (c Chameleon) BLightblue() Chameleon {
	return c.addColor("\x1b[104m")
}

// BLightmagenta returns the string in background light magenta
func (c Chameleon) BLightmagenta() Chameleon {
	return c.addColor("\x1b[105m")
}

// BLightcyan returns the string in background light cyan
func (c Chameleon) BLightcyan() Chameleon {
	return c.addColor("\x1b[106m")
}

// BWhite returns the string in background white
func (c Chameleon) BWhite() Chameleon {
	return c.addColor("\x1b[107m")
}

// AddCustomColor add a custom color
func (c Chameleon) AddCustomColor(colorname, color string) {
	cfcolor[colorname] = fmt.Sprintf("\033[" + color)
}

// CustomColor returns a custom color
func (c Chameleon) CustomColor(colorname string) Chameleon {
	return c.addColor(cfcolor[colorname])
}
