package hcicore

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"reflect"
	"regexp"
	"strings"
	"syscall"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/ssh"
)

// GetUUIDString creates and returns a new uuid
func GetUUIDString() (string, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return "", err
	}
	return id.String(), err
}

// IsValidUUIDString checks if a string is a valid uuid
func IsValidUUIDString(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}

// GetKeysFromStringMap returns the map keys as a new string slice
func GetKeysFromStringMap(m interface{}) []string {
	v := reflect.ValueOf(m)
	if v.Kind() != reflect.Map {
		return []string{}
	}
	keys := v.MapKeys()
	keysS := []string{}
	for _, k := range keys {
		keysS = append(keysS, fmt.Sprintf("%v", k.String()))
	}
	return keysS
}

// AskForConfirmation uses Scanln to parse user input. A user must type in "yes" or "no" and
// then press enter. It has fuzzy matching, so "y", "Y", "yes", "YES", and "Yes" all count as
// confirmations. If the input is not recognized, it will ask again. The function does not return
// until it gets a valid response from the user. Typically, you should print out a question
// before calling AskForConfirmation. E.g. fmt.Println("WARNING: Are you sure? (yes/no)")
func AskForConfirmation() bool {
	var response string
	_, err := fmt.Scanln(&response)
	if err != nil {
		return false
	}
	okayResponses := []string{"y", "Y", "yes", "Yes", "YES"}
	nokayResponses := []string{"n", "N", "no", "No", "NO"}
	if StringSliceContains(okayResponses, response, false) {
		return true
	} else if StringSliceContains(nokayResponses, response, false) {
		return false
	} else {
		fmt.Println("Please type yes or no and then press enter:")
		return AskForConfirmation()
	}
}

// ExecuteSingleCommandOnRemoteHost executes a given command and returns the output of the command
// returns the exit code and the error from the command
func ExecuteSingleCommandOnRemoteHost(cmd, user, sshKey, ip, port string) (string, int, error) {
	// privateKey could be read from a file, or retrieved from another storage
	// source, such as the Secret Service / GNOME Keyring
	file, err := os.Open(sshKey)
	if err != nil {
		return "", -1, err
	}
	defer file.Close()
	privateKey, err := ioutil.ReadAll(file)
	key, err := ssh.ParsePrivateKey([]byte(privateKey))
	if err != nil {
		return "", -1, err
	}
	// Authentication
	config := &ssh.ClientConfig{
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		User:            user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(key),
		},
	}
	// Connect
	client, err := ssh.Dial("tcp", ip+":"+port, config)
	if err != nil {
		return "", -1, err
	}
	// Create a session. It is one session per command.
	session, err := client.NewSession()
	if err != nil {
		return "", -1, err
	}
	defer session.Close()
	var stderr, stdout bytes.Buffer
	session.Stdout = &stdout // get output
	session.Stderr = &stderr
	err = session.Run(cmd)
	if exiterr, ok := err.(*exec.ExitError); ok {
		if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
			if status.ExitStatus() != 0 {
				if stderr.String() != "" {
					return "", status.ExitStatus(), errors.New(stderr.String())
				}
				return "", status.ExitStatus(), nil
			}
		}
	} else {
		if stderr.String() != "" {
			return "", -1, errors.New(stderr.String())
		}
		if err != nil {
			return "", -1, err
		}
	}
	return stdout.String(), 0, nil
}

// ExecuteSingleCommand executes a given command and returns the output of the command
// returns the exit code and the error from the command
func ExecuteSingleCommand(command string, arg ...string) (string, int, error) {
	cmd := exec.Command(command, arg...)
	var stderr, stdout bytes.Buffer
	cmd.Stderr = &stderr
	cmd.Stdout = &stdout
	err := cmd.Run()
	if exiterr, ok := err.(*exec.ExitError); ok {
		if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
			if status.ExitStatus() != 0 {
				if stderr.String() != "" {
					return "", status.ExitStatus(), errors.New(stderr.String())
				}
				return "", status.ExitStatus(), nil
			}
		}
	} else {
		if stderr.String() != "" {
			return "", -1, errors.New(stderr.String())
		}
		if err != nil {
			return "", -1, err
		}
	}
	return stdout.String(), 0, nil
}

// IsDirectoryEmpty returns true if the given path is completely empty
func IsDirectoryEmpty(name string) (bool, error) {
	f, err := os.Open(name)
	if err != nil {
		return false, nil
	}
	defer f.Close()

	// read in ONLY one file
	_, err = f.Readdir(1)

	// and if the file is EOF... well, the dir is empty.
	if err == io.EOF {
		return true, nil
	}
	return false, err
}

// RandomString returns a random string
func RandomString(n int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789")
	length := n
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}

// FindIPInString finds a ip in a string with other characters
func FindIPInString(input string) string {
	numBlock := "(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
	regexPattern := numBlock + "\\." + numBlock + "\\." + numBlock + "\\." + numBlock

	regEx := regexp.MustCompile(regexPattern)
	return regEx.FindString(input)
}

//StringSliceGetLastString returns the last string from a slice
func StringSliceGetLastString(ss []string) string {
	return ss[len(ss)-1]
}

//StringSliceContains checks a string slice for occurence of a string
func StringSliceContains(slice []string, toCheck string, casesensitive bool) bool {
	for _, s := range slice {
		if casesensitive {
			if toCheck == s {
				return true
			}
		} else {
			if strings.ToLower(toCheck) == strings.ToLower(s) {
				return true
			}
		}

	}

	return false
}

// StringSliceDifference returns the difference between two string slices
// BOOL swap = swap slice1 with slice2 in a second loop
func StringSliceDifference(slice1 []string, slice2 []string, swap bool) []string {
	var diff []string
	// Loop two times, first to find slice1 strings not in slice2,
	// second loop to find slice2 strings not in slice1
	for i := 0; i < 2; i++ {
		for _, s1 := range slice1 {
			found := false
			for _, s2 := range slice2 {
				if s1 == s2 {
					found = true
					break
				}
			}
			// String not found. We add it to return slice
			if !found {
				diff = append(diff, s1)
			}
		}
		// Only swap slices if swap bool is true
		if swap {
			// Swap the slices, only if it was the first loop
			if i == 0 {
				slice1, slice2 = slice2, slice1
			}
		}
	}
	return diff
}

// StringSliceRemoveValue remove a string from a slice
func StringSliceRemoveValue(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

// StringSliceGetSplitValueSlice returns a new slice
// where all items are splitted at the given seperator
// the index defindes which part of the splitted string should be added
// e.g. string = "test:blub" | StringSliceGetSplitValueSlice(slice, ":", 0) returns test
func StringSliceGetSplitValueSlice(slice []string, sep string, index int) []string {
	var splitSlice []string
	for _, s := range slice {
		splitSlice = append(splitSlice, strings.Split(s, sep)[index])
	}
	return splitSlice
}

// StringSliceRemoveKey remove a string from a slice via key
func StringSliceRemoveKey(s interface{}, i int) {
	vField := reflect.ValueOf(s)
	value := vField.Elem()
	if value.Kind() == reflect.Slice || value.Kind() == reflect.Array {
		result := reflect.AppendSlice(value.Slice(0, i), value.Slice(i+1, value.Len()))
		value.Set(result)
	}
}

// StringSliceDuplicates returns duplicate values in a slice
func StringSliceDuplicates(slice []string, casesensitive bool) []string {
	var rSlice []string
	for k, s := range slice {
		trimSlice := slice
		trimSlice[k] = ""
		if StringSliceContains(trimSlice, s, casesensitive) {
			rSlice = append(rSlice, s)
		}
	}
	return rSlice
}

// StringSliceFlip flips the string slice from AZ to ZA
func StringSliceFlip(s []string) []string {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}

// ConvertStream2String does the conversion
func ConvertStream2String(stream io.Reader) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.String()
}

//ConvertTime2Milliseconds does the conversion
func ConvertTime2Milliseconds(value time.Time) int64 {
	return value.UnixNano() / int64(time.Millisecond)
}

//ConvertMilliseconds2Time does the conversion
func ConvertMilliseconds2Time(millis int64) time.Time {
	return time.Unix(0, millis*int64(time.Millisecond))
}

// IfThenElse for oneline if
// use this only for small operations like formatting text
func IfThenElse(condition bool, a interface{}, b interface{}) interface{} {
	if condition {
		return a
	}
	return b
}
