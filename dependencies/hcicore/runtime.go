package hcicore

import (
	"runtime"
)

// GetCallstack retuns a frame slice.
//
// The argument skip is the number of stack frames to skip before recording,
// with 0 identifying the frame for GetCallstack itself and 1 identifying the caller of GetCallstack.
//
// The argument callstackDepth defines the max count of Frame items to be returned if available.
func GetCallstack(skip, callstackDepth int) []runtime.Frame {
	pc := make([]uintptr, callstackDepth)
	runtime.Callers(skip+1, pc) // ignore runtime.Callers
	frames := runtime.CallersFrames(pc)
	result := []runtime.Frame{}
	for {
		frame, more := frames.Next()
		result = append(result, frame)

		if !more {
			break
		}
	}
	return result
}
