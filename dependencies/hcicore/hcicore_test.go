package hcicore

import (
	"fmt"
	"strings"
	"testing"
)

func TestUUID(t *testing.T) {
	got, err := GetUUIDString()
	if err != nil {
		t.Error(err)
	}
	if got == "" {
		t.Errorf("GetUUIDString() = %s; want an uuid", got)
	}
	valid := IsValidUUIDString(got)
	t.Log(valid)
}

func TestStringSliceRemoveKey(t *testing.T) {
	mySlice := []string{"A", "B", "C", "D", "E"}
	StringSliceRemoveKey(&mySlice, 2)
	t.Log(strings.Join(mySlice, ", "))
}

func TestGetKeysFromStringMap(t *testing.T) {
	myMap := map[string]int{"KA": 1, "KB": 2, "KC": 3, "KD": 4, "KE": 5}
	keys := GetKeysFromStringMap(myMap)
	t.Log(strings.Join(keys, ", "))
}

func TestExecuteSingleCommand(t *testing.T) {
	out, code, err := ExecuteSingleCommand("sed", "-i", " a blub", "./testfile.txt")
	if err != nil {
		t.Error(err)
	}
	t.Log(out, code)
}

// comment from Martin: build a test env without need of /home files!!!
func TestExecuteSingleCommandOnRemoteHost(t *testing.T) {
	out, code, err := ExecuteSingleCommandOnRemoteHost("cat /etc/fstab", "root", "/home/stefan/.ssh/id_rsa", "10.200.26.207", "22")
	if err != nil {
		t.Error(err)
	}
	t.Log(out, code)
}

func TestStringSliceContains(t *testing.T) {
	//case insensitive
	got := StringSliceContains([]string{"a", "B", "c"}, "b", false)
	if !got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"b\", false) = %t; want true", got)
	}
	got = StringSliceContains([]string{"a", "B", "c"}, "B", false)
	if !got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"B\", false) = %t; want true", got)
	}
	got = StringSliceContains([]string{"a", "B", "c"}, "d", false)
	if got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"d\", false) = %t; want false", got)
	}
	got = StringSliceContains([]string{"a", "B", "c"}, "D", false)
	if got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"D\", false) = %t; want false", got)
	}

	//case sensitive
	got = StringSliceContains([]string{"a", "B", "c"}, "B", true)
	if !got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"B\", true) = %t; want true", got)
	}
	got = StringSliceContains([]string{"a", "B", "c"}, "b", true)
	if got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"b\", true) = %t; want false", got)
	}
	got = StringSliceContains([]string{"a", "B", "c"}, "D", true)
	if got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"D\", true) = %t; want false", got)
	}
	got = StringSliceContains([]string{"a", "B", "c"}, "d", true)
	if got {
		t.Errorf("StringSliceContains([]string{\"a\", \"B\", \"c\"}, \"d\", true) = %t; want false", got)
	}

}

func TestGetCallstack(t *testing.T) {
	got := GetCallstack(1, 25)

	if len(got) == 0 {
		t.Error("GetCallstack() returns empty; want a result")
	} else {
		fmt.Println("TestGetCallstack - visual test of result")
		for _, frame := range got {
			fmt.Printf("%+v\n", frame)
		}
		fmt.Println("TestGetCallstack ----")
	}
}
