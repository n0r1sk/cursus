package hcicore

import (
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

//LoadYamlfile loads the values from the yaml file into the interface
func LoadYamlfile(filename string, t interface{}) error {

	cfgdata, err := ioutil.ReadFile(filename)

	if err != nil {
		log.Error("Cannot open file from " + filename)
		return err
	}
	return UnmarshalYaml2Interface(cfgdata, t)
}

//UnmarshalYaml2Interface does the unmarshal operation
func UnmarshalYaml2Interface(yamlContent []byte, t interface{}) error {
	err := yaml.Unmarshal([]byte(yamlContent), t)
	if err != nil {
		log.Error("Cannot map yml file to interface, possible syntax error")
		return err

	}

	return nil
}
