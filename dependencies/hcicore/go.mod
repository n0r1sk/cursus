module git.app.strabag.com/dev/hostingci/gomodules/hcicore.git

go 1.16

require (
	github.com/google/uuid v1.3.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/sys v0.0.0-20210831042530-f4d43177bf5e // indirect
	gopkg.in/yaml.v2 v2.4.0
)
