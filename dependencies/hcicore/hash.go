package hcicore

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"os"
)

//HashFileMD5 returns the md5 hash of the file by specified by filename
func HashFileMD5(filename string) (string, error) {

	var returnMD5String string

	file, err := os.Open(filename)
	if err != nil {
		return returnMD5String, err
	}

	defer file.Close()

	hash := md5.New()

	if _, err := io.Copy(hash, file); err != nil {
		return returnMD5String, err
	}

	hashInBytes := hash.Sum(nil)[:16]

	returnMD5String = hex.EncodeToString(hashInBytes)

	return returnMD5String, nil
}
