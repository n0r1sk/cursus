module git.app.strabag.com/dev/hostingci/gomodules/hcilog.git

go 1.15

require (
	git.app.strabag.com/dev/hostingci/gomodules/hcicore.git v1.5.0
	git.app.strabag.com/dev/hostingci/gomodules/hcielastic.git v1.3.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/olivere/elastic/v7 v7.0.20
	github.com/sirupsen/logrus v1.7.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
