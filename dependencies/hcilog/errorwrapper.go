package hcilog

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
	log "github.com/sirupsen/logrus"
)

type errorwrapper struct {
	uuid           string
	logged         bool
	err            error
	callstack      string
	altMsg         string
	httpStatus     int
	additionalInfo []string
}

//ErrorWrapper is an instance for errorhandling
type ErrorWrapper struct {
	*errorwrapper
}

//ErrorWrapperJSON is a representiation of errorwrapper
type ErrorWrapperJSON struct {
	UUID         string   `json:"uuid,omitempty"`
	ErrStr       string   `json:"errormsg,omitempty"`
	Callstack    string   `json:"callstack,omitempty"`
	AltMsg       string   `json:"altmsg,omitempty"`
	HTTPStatus   int      `json:"httpstatus,omitempty"`
	AdditionInfo []string `json:"additionalinfo,omitempty"`
}

func newErrorWrapperInternal(callstack string, err error, additionalInfo ...string) *ErrorWrapper {
	uuid, uuidErr := hcicore.GetUUIDString()
	if uuidErr != nil {
		log.Error(uuidErr)
		uuid = "1L"
	}
	return &ErrorWrapper{&errorwrapper{uuid: uuid, err: err, callstack: callstack, httpStatus: http.StatusInternalServerError, additionalInfo: additionalInfo}}
}

// NewErrorWrapper creates a new NewErrorWrapper with the given error, the addtionalInfo and sets its httpStatus to http.StatusInternalServerError
func NewErrorWrapper(err error, additionalInfo ...string) *ErrorWrapper {
	return newErrorWrapperInternal(GetCallstack4Error(1), err, additionalInfo...)
}

// NewErrorWrapperAltMsg creates a new NewErrorWrapper with the given error, the altMsg, the addtionalInfo and sets its httpStatus to http.StatusInternalServerError
func NewErrorWrapperAltMsg(err error, altMsg string, additionalInfo ...string) *ErrorWrapper {
	instance := NewErrorWrapper(err, additionalInfo...)
	instance.altMsg = altMsg
	return instance
}

// NewErrorWrapperBadRequest creates a new NewErrorWrapper with the given error, the addtionalInfo and sets its httpStatus to http.StatusBadRequest
func NewErrorWrapperBadRequest(err error, additionalInfo ...string) *ErrorWrapper {
	instance := NewErrorWrapper(err, additionalInfo...)
	instance.httpStatus = http.StatusBadRequest
	return instance
}

// NewErrorWrapperForbidden creates a new NewErrorWrapper with the given error, the addtionalInfo and sets its httpStatus to http.StatusForbidden
func NewErrorWrapperForbidden(err error, additionalInfo ...string) *ErrorWrapper {
	instance := NewErrorWrapper(err, additionalInfo...)
	instance.httpStatus = http.StatusForbidden
	return instance
}

// NewErrorWrapperFromJSON creates a new NewErrorWrapper from given reader
func NewErrorWrapperFromJSON(r io.Reader) *ErrorWrapper {
	jsonObj := &ErrorWrapperJSON{}
	err := json.NewDecoder(r).Decode(jsonObj)
	if err != nil {
		return NewErrorWrapper(err)
	}
	instance := newErrorWrapperInternal(jsonObj.Callstack, errors.New(jsonObj.ErrStr), jsonObj.AdditionInfo...)
	instance.OverrideUUID(jsonObj.UUID)
	instance.SetAltMsg(jsonObj.AltMsg)
	instance.SetHTTPStatuscode(jsonObj.HTTPStatus)

	return instance
}

// LogError logs the containing error if not previously logged
func (e ErrorWrapper) LogError() {
	if e.err != nil && !e.logged {
		logErrorInternal(e.err, e.callstack, e.uuid, ConvertAddtionalInfo2String(e.additionalInfo))

		e.logged = true
	}
}

// String prints the error
func (e ErrorWrapper) String() string {
	e.LogError()
	if e.altMsg != "" {
		return fmt.Sprintf("%s (%s)", e.altMsg, e.uuid)
	}

	return fmt.Sprintf("%s (%s)", e.err.Error(), e.uuid)
}

// String4Page prints the error with ruleset for web pages
func (e ErrorWrapper) String4Page() string {
	e.LogError()
	if e.altMsg != "" {
		if e.httpStatus == http.StatusBadRequest {
			return fmt.Sprintf("%s", e.altMsg)
		}
		return fmt.Sprintf("%s (%s)", e.altMsg, e.uuid)
	}

	if e.httpStatus == http.StatusBadRequest {
		return fmt.Sprintf("%s", e.err.Error())
	}
	return fmt.Sprintf("%s (%s)", e.err.Error(), e.uuid)
}

//ToJSON returns JSON representation
func (e ErrorWrapper) ToJSON() (string, *ErrorWrapper) {
	e.LogError()
	json, err := json.Marshal(&ErrorWrapperJSON{UUID: e.uuid, ErrStr: e.err.Error(), Callstack: e.callstack, AltMsg: e.altMsg, HTTPStatus: e.httpStatus, AdditionInfo: e.additionalInfo})
	if err != nil {
		return "", NewErrorWrapper(err)
	}
	return fmt.Sprintf("%s", json), nil
}

//GetUUID returns the uuid
func (e ErrorWrapper) GetUUID() string {
	return e.uuid
}

//OverrideUUID sets the uuid to the given one
func (e ErrorWrapper) OverrideUUID(overrideUUID string) {
	e.uuid = overrideUUID
}

//GetError returns the error object
func (e ErrorWrapper) GetError() error {
	return e.err
}

//GetCallStack returns the callstack
func (e errorwrapper) GetCallStack() string {
	return e.callstack
}

//GetAdditionalInfo returns the additional infos as string slice
func (e ErrorWrapper) GetAdditionalInfo() []string {
	return e.additionalInfo
}

//AddAdditionalInfo appends strings to additional infos
func (e ErrorWrapper) AddAdditionalInfo(additionalInfo ...string) {
	e.additionalInfo = append(e.additionalInfo, additionalInfo...)
}

//GetAltMsg returns the altMsg
func (e ErrorWrapper) GetAltMsg() string {
	return e.altMsg
}

//SetAltMsg sets the altMsg to the given one
func (e ErrorWrapper) SetAltMsg(msg string) {
	e.altMsg = msg
}

//GetHTTPStatuscode returns the http status code
func (e ErrorWrapper) GetHTTPStatuscode() int {
	return e.httpStatus
}

//SetHTTPStatuscode sets the httpStatus to the given one
func (e ErrorWrapper) SetHTTPStatuscode(state int) {
	e.httpStatus = state
}
