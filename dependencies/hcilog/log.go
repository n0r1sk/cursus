package hcilog

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcielastic.git"
	log "github.com/sirupsen/logrus"
)

//DefaultErrorCallstackDepth defines the maximum count of callstack entries (number of calls)
const (
	DefaultErrorCallstackDepth int    = 25
	timestampFormat            string = "2006-01-02 15:04:05"
	dataKeyApp                 string = "app"
	dataKeyAdditionalinfo      string = "additionalinfo"
	dataKeyErrorUUID           string = "erroruuid"
	dataKeyCallStack           string = "callstack"
)

var (
	app               string
	callstackDepth    int
	dataKeyRestricted = []string{}
)

func init() {
	callstackDepth = DefaultErrorCallstackDepth

	dataKeyRestricted = append(dataKeyRestricted, dataKeyApp)
	dataKeyRestricted = append(dataKeyRestricted, dataKeyAdditionalinfo)
	dataKeyRestricted = append(dataKeyRestricted, dataKeyErrorUUID)
	dataKeyRestricted = append(dataKeyRestricted, dataKeyCallStack)
}

// GetLogTimestamp returns the current timestamp applicable for elastic logging
func GetLogTimestamp() string {
	return time.Now().UTC().Format(time.RFC3339Nano)
}

//InitializeLog for the project
func InitializeLog(appname string, debug bool) {
	if appname == "" {
		log.Panic("Appname MUST NOT be empty")
	}
	app = appname
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = timestampFormat
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)
	if debug {
		log.SetLevel(log.DebugLevel)
	}

}

//InitializeElasticLog sets up the elastic system with the configuration and adds the elastic hook to logrus
func InitializeElasticLog(elasticconfigFilepath string) {
	err := hcielastic.InitializeElasticConfig(elasticconfigFilepath)
	if err != nil {
		log.Panic(err)
	}

	host, err := os.Hostname()
	if err != nil {
		log.Panic(err)
	}

	logIndex, err := hcielastic.GetElasticIndex(hcielastic.ElasticPurposeLog)
	if err != nil {
		log.Panic(err)
	}
	hook, err := NewAsyncElasticHookWithFunc(logIndex.Client, host, log.InfoLevel, func() string {
		return logIndex.Name
	})
	if err != nil {
		log.Panic(err)
	}
	log.AddHook(hook)
}

//EvalDebugEnvironment returns whether the environment is set to debug
//Pass the kingpin debug flag as parameter
func EvalDebugEnvironment(commandLinedebug bool) bool {
	if commandLinedebug {
		return commandLinedebug
	}

	debug := false
	debugStr := os.Getenv("debug")

	if debugStr != "" {
		b, err := strconv.ParseBool(debugStr)
		if err == nil {
			debug = b
		}

	}
	return debug
}

//GetCallstack4Error gets the callstack and formats it to string
func GetCallstack4Error(callstackSkip int) string {
	frames := hcicore.GetCallstack(callstackSkip+1, callstackDepth)
	callstackStr := ""
	for index, frame := range frames {
		if index > 0 {
			callstackStr = fmt.Sprintf("%s\n", callstackStr)
		}

		callstackStr = fmt.Sprintf("%s%s in %s Line: %d", callstackStr, frame.Function, frame.File, frame.Line)
	}
	return callstackStr
}

//ConvertAddtionalInfo2String converts the slice to a joined string
func ConvertAddtionalInfo2String(additionalInfo []string) string {
	if len(additionalInfo) > 0 {
		return strings.Join(additionalInfo, " - ")
	}
	return ""
}

func logErrorInternal(err error, callstack, errorUUID, additionalInfo string) {
	logger := log.WithFields(log.Fields{dataKeyApp: app, dataKeyCallStack: callstack, dataKeyErrorUUID: errorUUID, dataKeyAdditionalinfo: additionalInfo})
	logger.Error(err)
}

// LogError logs an error
func LogError(err error, additionalInfo ...string) {
	callstack := GetCallstack4Error(1)

	uuid, err := hcicore.GetUUIDString()
	if err != nil {
		logErrorInternal(err, callstack, "", "UUID generation faild")
		return
	}
	logErrorInternal(err, callstack, uuid, ConvertAddtionalInfo2String(additionalInfo))
}

//LogWarn logs a warning
func LogWarn(message string) {
	logger := log.WithFields(log.Fields{dataKeyApp: app})
	logger.Warn(message)
}

//LogWarnA logs a warning with additionalInfo
func LogWarnA(message, additionalInfo string) {
	logger := log.WithFields(log.Fields{dataKeyApp: app, dataKeyAdditionalinfo: additionalInfo})
	logger.Warn(message)
}

//LogInfo logs an info
func LogInfo(message string) {
	logger := log.WithFields(log.Fields{dataKeyApp: app})
	logger.Info(message)
}

//LogInfoA logs an info with additionalInfo
func LogInfoA(message, additionalInfo string) {
	logger := log.WithFields(log.Fields{dataKeyApp: app, dataKeyAdditionalinfo: additionalInfo})
	logger.Info(message)
}

//LogDebug logs a debug information
func LogDebug(message string) {
	logger := log.WithFields(log.Fields{dataKeyApp: app})
	logger.Debug(message)
}

//LogDebugA logs a debug information with additionalInfo
func LogDebugA(message, additionalInfo string) {
	logger := log.WithFields(log.Fields{dataKeyApp: app, dataKeyAdditionalinfo: additionalInfo})
	logger.Debug(message)
}
