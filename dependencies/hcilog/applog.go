package hcilog

import (
	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcielastic.git"
)

// BaseAppLog basic data
type BaseAppLog struct {
	Data      map[string]string
	Timestamp string `json:"@timestamp"`
	ID        string `json:"id"`
}

//DoAppLog writes the logObj as index to the Elastic cluster
func DoAppLog(logObj interface{}) {
	hcielastic.DoStream(hcielastic.ElasticPurposeApplog, logObj)
}

//CreateBaseAppLog creates the obj and fills the basic data
func CreateBaseAppLog() *BaseAppLog {

	uuid, err := hcicore.GetUUIDString()
	if err != nil {
		LogError(err)
	}
	baseAppLog := BaseAppLog{Data: make(map[string]string), ID: uuid, Timestamp: GetLogTimestamp()}
	baseAppLog.Data[dataKeyApp] = app

	return &baseAppLog
}

//OverrideUUID sets the uuid to the given one
//ONLY USE in special cases for exampe multiple applogs which belong together - shall be found in elastic "together"
func (baseAppLog *BaseAppLog) OverrideUUID(overrideUUID string) {
	baseAppLog.Data[dataKeyErrorUUID] = overrideUUID
}

// AppendData appends key,value combination to baseAppLog returns success/failure
// Cant append restricted keys or existing keys
func (baseAppLog *BaseAppLog) AppendData(key, value string) bool {
	if hcicore.StringSliceContains(dataKeyRestricted, key, false) {
		return false
	}
	if _, ok := baseAppLog.Data[key]; ok {
		//already in data
		return false
	}
	baseAppLog.Data[key] = value
	return true
}

// AppendErrorWarpperData appends the standarized values form the errorwrapper to the baseAppLog
func (baseAppLog *BaseAppLog) AppendErrorWarpperData(ew *ErrorWrapper) {
	baseAppLog.Data[dataKeyErrorUUID] = ew.GetUUID()
}
