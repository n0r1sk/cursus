package hcielastic

import (
	"context"
	"errors"
	"fmt"
	"time"

	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
	"github.com/olivere/elastic/v7"
	log "github.com/sirupsen/logrus"
)

//ElasticPurposeLog is the key for log index
const ElasticPurposeLog = "log"

//ElasticPurposeApplog is the key for applog index
const ElasticPurposeApplog = "applog"
const indexDatePattern = "2006.01.02"
const maxRetrys int = 3
const preinitTimeout int = 500
const initTryTimeout int = 1000

//ElasticUser holds user info
type ElasticUser struct {
	Name     string
	Password string
}

//ElasticIndex holds index information
type ElasticIndex struct {
	Client         *elastic.Client
	User           *ElasticUser
	Name           string
	UseDatePattern bool
}

type runningConfig struct {
	elasticURL string
	indices    map[string]*ElasticIndex
}

var (
	elastics *runningConfig
)

//GetElasticIndex returns the elastic index representation
//Checks the elastic client and initializes it if necessary
func GetElasticIndex(purpose string) (*ElasticIndex, error) {
	err := InitializeElasticClient(purpose)
	if err != nil {
		return nil, err
	}
	return elastics.indices[purpose], nil
}

//BuildIndexname creates the name used to create the actual index for sending to elastic
func BuildIndexname(index *ElasticIndex) string {
	name := index.Name
	if index.UseDatePattern {
		name = fmt.Sprintf("%s-%s", index.Name, time.Now().UTC().Format(indexDatePattern))
	}
	return name
}

//InitializeElasticConfig parses the elsatic configuration and sets up the clients
func InitializeElasticConfig(filepath string) error {
	yaml, err := loadElasticConfigYaml(filepath)
	if err != nil {
		return err
	}

	if yaml.ElasticURL == "" {
		return errors.New("No elastic URL provided")
	}

	elastics = &runningConfig{elasticURL: yaml.ElasticURL, indices: make(map[string]*ElasticIndex)}
	for _, userConfig := range yaml.User {
		for _, index := range userConfig.Indices {
			elastics.indices[index.Purpose] = &ElasticIndex{User: &ElasticUser{Name: userConfig.Name, Password: userConfig.Password}, Name: index.Name, UseDatePattern: index.UseDatePattern}
		}
	}

	return nil
}

//InitializeElasticClient creates the elastic client and validates the connectivity to elastic if not already initialized
//Elastic user MUST at least have read index right
func InitializeElasticClient(purpose string) error {
	if elastics == nil {
		return errors.New("Elastic configuration not initialized! Call InitializeElasticConfig(filepath string) once before calling this function")
	}
	index, ok := elastics.indices[purpose]
	if !ok {
		return fmt.Errorf("No index configuration provided for purpose %s", purpose)
	}
	if index.Client == nil {
		//wait a little bit for network being up
		log.Debugf("Waiting before elastic initialization %d milliseconds", preinitTimeout)
		time.Sleep(time.Duration(preinitTimeout) * time.Millisecond)

		tryCount := 0
		for tryCount < maxRetrys {

			var elasticErr error
			index.Client, elasticErr = elastic.NewClient(elastic.SetHealthcheck(false), elastic.SetSniff(false), elastic.SetURL(elastics.elasticURL), elastic.SetBasicAuth(index.User.Name, index.User.Password))
			if elasticErr != nil {
				if tryCount >= maxRetrys {
					log.Panic(elasticErr)
				} else {
					tryCount++
					//wait between trys
					log.Warnf("Waiting before next elastic initialization try %d milliseconds", initTryTimeout)
					time.Sleep(time.Duration(initTryTimeout) * time.Millisecond)
					continue
				}
			}
			break
		}
	}

	indexname4Check := BuildIndexname(index)

	log.Debugf("Elastic checking connectivity with index: %s", indexname4Check)
	for true {
		exists, err := index.Client.IndexExists(indexname4Check).Do(context.Background())
		if err != nil {
			log.Error(err)

			log.Warnf("Waiting before next elastic connectivity try %d milliseconds", initTryTimeout)
			time.Sleep(time.Duration(initTryTimeout) * time.Millisecond)
		} else {
			log.Debugf("Connectivity checked, index exists: %t\n", exists)
			break
		}
	}

	return nil

}

// GetHitsTotalFromIndex returns the size of an index
func GetHitsTotalFromIndex(purposeIndex, index string) (int64, error) {
	indexObj, err := GetElasticIndex(purposeIndex)
	if err != nil {
		return 0, err
	}
	out, err := indexObj.Client.Search(index).Do(context.Background())
	if err != nil {
		return 0, err
	}
	return out.Hits.TotalHits.Value, nil
}

// GetIndexDocument returns the index document
// size optional default. 10000
func GetIndexDocument(purposeIndex, index string, size int) ([]*elastic.SearchHit, error) {
	indexObj, err := GetElasticIndex(purposeIndex)
	if size == 0 || size == -1 {
		size = 10000
	}
	if err != nil {
		return nil, err
	}
	out, err := indexObj.Client.Search(index).Size(size).Do(context.Background())
	if err != nil {
		return nil, err
	}
	return out.Hits.Hits, nil
}

//DoIndex writes an obj as index to the Elastic cluster
func DoIndex(elasticPurpose string, logObj interface{}) {
	index, err := GetElasticIndex(elasticPurpose)
	if err != nil {
		log.Error(err)
		return
	}

	uuid, err := hcicore.GetUUIDString()

	if err != nil {
		log.Error()
		return
	}
	_, err = index.Client.Index().
		Index(BuildIndexname(index)).
		Id(uuid).
		BodyJson(logObj).
		Do(context.Background())
	if err != nil {
		elasticError, ok := err.(*elastic.Error)
		if ok {
			log.Error(err, fmt.Sprintf("%+v", elasticError.Details.CausedBy))
		} else {
			log.Error(err)
		}
	}

}

//DoStream writes an obj to a stream of the Elastic cluster
func DoStream(elasticPurpose string, logObj interface{}) {
	index, err := GetElasticIndex(elasticPurpose)
	if err != nil {
		log.Error(err)
		return
	}

	_, err = index.Client.Index().
		Index(index.Name).
		Type("_doc").
		BodyJson(logObj).
		Do(context.Background())
	if err != nil {
		elasticError, ok := err.(*elastic.Error)
		if ok {
			log.Error(err, fmt.Sprintf("%+v", elasticError.Details.CausedBy))
		} else {
			log.Error(err)
		}
	}

}
