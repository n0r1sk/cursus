module git.app.strabag.com/dev/hostingci/gomodules/hcielastic.git

go 1.15

require (
	git.app.strabag.com/dev/hostingci/gomodules/hcicore.git v1.5.0
	github.com/olivere/elastic/v7 v7.0.20
	github.com/sirupsen/logrus v1.7.0
)
