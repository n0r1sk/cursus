package hcielastic

import "git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"

type elasticIndexConfig struct {
	Name           string
	Purpose        string
	UseDatePattern bool
}

type elasticUserConfig struct {
	Name     string
	Password string
	Indices  []elasticIndexConfig
}

type elasticConfigYaml struct {
	ElasticURL string
	User       []elasticUserConfig
}

func loadElasticConfigYaml(filepath string) (*elasticConfigYaml, error) {
	config := &elasticConfigYaml{}
	err := hcicore.LoadYamlfile(filepath, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
