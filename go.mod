module cursus

go 1.17

replace (
	git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git => ./dependencies/hcichameleon
	git.app.strabag.com/dev/hostingci/gomodules/hcicore.git => ./dependencies/hcicore
	git.app.strabag.com/dev/hostingci/gomodules/hcielastic.git => ./dependencies/hcielastic
	git.app.strabag.com/dev/hostingci/gomodules/hcilog.git => ./dependencies/hcilog
)

require (
	git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git v1.0.6
	git.app.strabag.com/dev/hostingci/gomodules/hcicore.git v1.7.0
	git.app.strabag.com/dev/hostingci/gomodules/hcielastic.git v1.5.0 // indirect
	git.app.strabag.com/dev/hostingci/gomodules/hcilog.git v1.7.0
	github.com/GoogleCloudPlatform/cloudsql-proxy v1.28.0
	github.com/Jeffail/gabs v1.4.0
	github.com/atotto/clipboard v0.1.4
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/olivere/elastic/v7 v7.0.31 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.20.0 // indirect
	golang.org/x/net v0.0.0-20220114011407-0dd24b26b47d // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/api v0.65.0 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)

require (
	cloud.google.com/go/compute v1.1.0 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/googleapis/gax-go/v2 v2.1.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	go.opencensus.io v0.23.0 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20220118154757-00ab72f36ad5 // indirect
	google.golang.org/grpc v1.43.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
