#!/bin/bash
zip build/cursus-linux-386.zip build/cursus-linux-386
zip build/cursus-linux-amd64.zip build/cursus-linux-amd64
zip build/cursus-linux-amd64-static.zip build/cursus-linux-amd64-static
zip build/cursus-linux-arm.zip build/cursus-linux-arm
zip build/cursus-linux-arm64.zip build/cursus-linux-arm64
zip build/cursus-darwin-amd64.zip build/cursus-darwin-amd64
export LINUX_386_BIN_URL=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --form "file=@build/cursus-linux-386.zip" "https://gitlab.com/api/v4/projects/20843292/uploads" | jq -r '.full_path')
export LINUX_AMD64_BIN_URL=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --form "file=@build/cursus-linux-amd64.zip" "https://gitlab.com/api/v4/projects/20843292/uploads" | jq -r '.full_path')
export LINUX_AMD64_STATIC_BIN_URL=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --form "file=@build/cursus-linux-amd64-static.zip" "https://gitlab.com/api/v4/projects/20843292/uploads" | jq -r '.full_path')
export LINUX_ARM_BIN_URL=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --form "file=@build/cursus-linux-arm.zip" "https://gitlab.com/api/v4/projects/20843292/uploads" | jq -r '.full_path')
export LINUX_ARM64_BIN_URL=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --form "file=@build/cursus-linux-arm64.zip" "https://gitlab.com/api/v4/projects/20843292/uploads" | jq -r '.full_path')
export DARWIN_BIN_URL=$(curl --silent --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --form "file=@build/cursus-darwin-amd64.zip" "https://gitlab.com/api/v4/projects/20843292/uploads" | jq -r '.full_path')
export DESCRIPTION=$(echo $CI_COMMIT_MESSAGE | tr '\n' ' ')

release-template/gomplate -f release-template/release.tpl -o release-template/release.sh
chmod u+x release-template/release.sh
release-template/release.sh